// Copyright (c) 2012 Robert W Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package httprouter

import (
	"fmt"
	"net/url"
	"testing"
)

func ExamplePathNth() {
	// An example path in a request
	path := "/diary/user/2012-12-25/1"

	// breakout the elements
	username := PathNth(path, 1)
	entrydate := PathNth(path, 2)
	entryindex := PathNth(path, 3)

	// show the results
	fmt.Println("User:", username)
	fmt.Println("Entry date:", entrydate)
	fmt.Println("Entry index:", entryindex)

	//Output:
	//User: user
	//Entry date: 2012-12-25
	//Entry index: 1
}

func ExampleUrlNth() {
	// An example path in a request
	url, err := url.Parse("http://localhost/diary/user/2012-12-25/1")
	if err != nil {
		panic(err)
	}

	// breakout the elements
	username := UrlNth(url, 1)
	entrydate := UrlNth(url, 2)
	entryindex := UrlNth(url, 3)

	// show the results
	fmt.Println("User:", username)
	fmt.Println("Entry date:", entrydate)
	fmt.Println("Entry index:", entryindex)

	//Output:
	//User: user
	//Entry date: 2012-12-25
	//Entry index: 1
}

func TestPathNth(t *testing.T) {
	cases := []struct {
		path  string
		nth   int
		value string
	}{
		{"/a/b/c/", 0, "a"},
		{"/a/b/c/", 1, "b"},
		{"/a/b/c/", 2, "c"},
		{"/a/b/c/", 3, ""},
		{"a/b/c/", 0, "a"},
		{"a/b/c/", 1, "b"},
		{"a/b/c/", 2, "c"},
		{"/a/b/c", 0, "a"},
		{"/a/b/c", 1, "b"},
		{"/a/b/c", 2, "c"},
		{"a/b/c", 0, "a"},
		{"a/b/c", 1, "b"},
		{"a/b/c", 2, "c"},
	}

	for _, v := range cases {
		ret := PathNth(v.path, v.nth)
		if ret != v.value {
			t.Errorf("Incorrect nth element %s, %d -> %s", v.path, v.nth, ret)
		}
	}
}
