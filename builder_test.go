// Copyright (c) 2012 Robert W Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package httprouter

import (
	"fmt"
	"net/http"
	"testing"
)

func ExampleNewBuilder() {
	// Create a builder to start construction of our routing policy.
	b := NewBuilder()

	// The login page will be special.  A get method will return an HTML
	// login page, but we'll post our credentials to login.
	b.MustHandleFunc("/login", GetMethod, func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "<html><body>A login page.</body></html>")
	})
	b.MustHandleFunc("/login", PostMethod, func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "<html><body>You're logged in!</body></html>")
	})
	// Our content will be organized by date, which will be part
	// of our URL.
	b.MustHandleFunc("/content/\\d{4}-\\d{2}-\\d{2}/?", GetMethod, func(w http.ResponseWriter, r *http.Request) {
		date := r.URL.Path[9:19] // A string of the form YYYY-MM-DD
		fmt.Fprintf(w, "<html><body>Some content for %s.</body></html>", date)
	})

	// Construct a ServeMux.  This will construct the DFA based on the
	// requested patterns that can be used for routing.
	mux, err := b.NewServeMux()
	if err != nil {
		// If you have conflicting routes, you will find out here
		panic(err)
	}

	// Start serving content.
	_ = http.ListenAndServe(":8000", mux)
}

func ExampleNewBuilder_conflict() {
	// Create a new builder to start construction of our routing policy.
	b := NewBuilder()

	// Our first route will be a fixed login page.
	b.MustHandleFunc("/login", GetMethod, func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "<html><body>A login page.</body></html>")
	})
	// Our second route will match all routes that start with "log".
	b.MustHandleFunc("/log.+", GetMethod, func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "<html><body>A login page.</body></html>")
	})

	// Construct a ServeMux.  Unfortunately, ou routes conflict since there are
	// paths that match multiple routes.
	_, err := b.NewServeMux()
	fmt.Println("error:", err)

	// Output:
	// error: httprouter: conflict in an accept state: "/log.+" and "/login"
}

func ExampleNewBuilder_trailing_slash() {
	// Create a new builder to start construction of our routing policy.
	b := NewBuilder()

	// Our first route will be a fixed login page.
	// Note the use of the '?' operator.  The trailing slash will be optional.
	b.MustHandleFunc("/login/?", GetMethod, func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "<html><body>A login page.</body></html>")
	})

	// Construct a ServeMux.
	_, _ = b.NewServeMux()
}

func ExampleNewBuilder_alpha() {
	// Create a new builder to start construction of our routing policy.
	b := NewBuilder()

	// Our first route will be a fixed login page, but the username must be
	// specified.  Note that any sequence of one or more alpha characters will
	// match.
	b.MustHandleFunc("/login/[[:alpha:]]+", GetMethod, func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "<html><body>A login page.</body></html>")
	})

	// Construct a ServeMux.
	_, _ = b.NewServeMux()
}

func TestBuilder_Handle(t *testing.T) {
	cases := []struct {
		in string
		ok bool
	}{
		{"/login", true},
		{"(/login)|(/logout)", true},
		{"[a-z]+", true},
		{"[a-z]++", false},
	}

	t.Run("Handle", func(t *testing.T) {
		for _, v := range cases {
			t.Run(v.in, func(t *testing.T) {
				b := NewBuilder()
				err := b.Handle(v.in, AllMethods, nil)
				if ok := err == nil; ok != v.ok {
					t.Errorf("got %v, want %v", ok, v.ok)
				}
			})
		}
	})

	t.Run("MustHandle", func(t *testing.T) {
		for _, v := range cases {
			t.Run(v.in, func(t *testing.T) {
				defer func() {
					r := recover()
					if ok := r == nil; ok != v.ok {
						t.Errorf("got %v, want %v", ok, v.ok)
					}
				}()

				b := NewBuilder()
				b.MustHandle(v.in, AllMethods, nil)
			})
		}
	})

	t.Run("HandleFunc", func(t *testing.T) {
		for _, v := range cases {
			t.Run(v.in, func(t *testing.T) {
				b := NewBuilder()
				err := b.HandleFunc(v.in, AllMethods, nil)
				if ok := err == nil; ok != v.ok {
					t.Errorf("got %v, want %v", ok, v.ok)
				}
			})
		}
	})

	t.Run("MustHandleFunc", func(t *testing.T) {
		for _, v := range cases {
			t.Run(v.in, func(t *testing.T) {
				defer func() {
					r := recover()
					if ok := r == nil; ok != v.ok {
						t.Errorf("got %v, want %v", ok, v.ok)
					}
				}()

				b := NewBuilder()
				b.MustHandleFunc(v.in, AllMethods, nil)
			})
		}
	})
}

func TestBuilder_NewServeMux(t *testing.T) {
	t.Run("empty", func(t *testing.T) {
		b := NewBuilder()
		mux, err := b.NewServeMux()
		if err == nil {
			t.Errorf("NewServeMux should have returned an error when empty.")
		}
		if mux != nil {
			t.Errorf("NewServeMux should have returned nil when empty.")
		}
	})

	t.Run("/login", func(t *testing.T) {
		b := NewBuilder()
		b.MustHandleFunc("/login", AllMethods, func(w http.ResponseWriter, r *http.Request) {
			println("Dummy method")
		})

		mux, err := b.NewServeMux()
		if err != nil {
			t.Errorf("NewServeMux unexpectedly returned an error: %s", err)
		}
		if mux == nil {
			t.Errorf("NewServeMux should have returned a non-nil result.")
		}
	})

	t.Run("/login|/logout", func(t *testing.T) {
		b := NewBuilder()
		b.MustHandleFunc("/login", AllMethods, func(w http.ResponseWriter, r *http.Request) {
			println("Dummy method")
		})
		b.MustHandleFunc("/logout", AllMethods, func(w http.ResponseWriter, r *http.Request) {
			println("Dummy method")
		})

		mux, err := b.NewServeMux()
		if err != nil {
			t.Errorf("NewServeMux unexpectedly returned an error: %s", err)
		}
		if mux == nil {
			t.Errorf("NewServeMux should have returned a non-nil result.")
		}
	})

	t.Run("conflict", func(t *testing.T) {
		b := NewBuilder()
		b.MustHandleFunc("/login", AllMethods, func(w http.ResponseWriter, r *http.Request) {
			println("Dummy method")
		})
		b.MustHandleFunc("/log.n", AllMethods, func(w http.ResponseWriter, r *http.Request) {
			println("Dummy method")
		})

		mux, err := b.NewServeMux()
		if err == nil {
			t.Fatalf("NewServeMux should have returned an error because of conflict.")
		}
		if _, ok := err.(ErrAcceptConflict); !ok {
			t.Errorf("NewServeMux should have returned an error because of conflict.")
		}
		if mux != nil {
			t.Errorf("NewServeMux should have returned nil because of conflict.")
		}
	})
}
