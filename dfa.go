// Copyright (c) 2012 Robert W Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package httprouter

import (
	"fmt"
	"sort"
)

// An ErrAcceptConflict will be returned when building the state machine if it
// is possible for an URL to match multiple handlers.  The regular expressions
// must match distinct sets of string.
type ErrAcceptConflict struct {
	accept [2]string
}

func (e ErrAcceptConflict) Error() string {
	// Make sure that the accept strings are sorted.  This creates a canonical
	// form for the error string.
	sort.Strings(e.accept[:])
	// Format the error.
	return fmt.Sprintf("httprouter: conflict in an accept state: %q and %q", e.accept[0], e.accept[1])
}

func isMember(states []int, n int) bool {
	if len(states) == 0 {
		return false
	}
	if len(states) == 1 {
		return states[0] == n
	}
	pivot := len(states) / 2
	if states[pivot] > n {
		return isMember(states[:pivot], n)
	}
	if states[pivot] < n {
		return isMember(states[pivot+1:], n)
	}
	// assert states[pivot]==n
	return true
}

func addState(states []int, n int) []int {
	if isMember(states, n) {
		return states
	}

	states = append(states, n)
	sort.Ints(states)
	return states
}

func eps_closure(states []int, n int, nfa []nfaNode) []int {
	for _, v := range nfa[n].epsilons {
		if !isMember(states, v) {
			states = append(states, v)
			sort.Ints(states)
			states = eps_closure(states, v, nfa)
		}
	}
	return states
}

func determineAccept(nodes []int, nfa []nfaNode) (accept string, err error) {
	for _, v := range nodes {
		if nfa[v].accept != "" {
			if accept != "" {
				return "", ErrAcceptConflict{[2]string{accept, nfa[v].accept}}
			}
			accept = nfa[v].accept
		}
	}
	return accept, nil
}

func buildKey(states []int) string {
	key := make([]byte, len(states)*4)

	for i, v := range states {
		key[i*4+0] = byte(v & 0xFF)
		key[i*4+1] = byte((v >> 8) & 0xFF)
		key[i*4+2] = byte((v >> 16) & 0xFF)
		key[i*4+3] = byte((v >> 24) & 0xFF)
	}
	return string(key)
}

type dfaedge struct {
	low, hi byte
	target  int
}

type dfastate struct {
	handler   *handler
	nfanodes  []int
	edgesHead dfaedge
	edges     []dfaedge
}

func (s *dfastate) Move(e byte) (state int, ok bool) {
	if e >= s.edgesHead.low && e <= s.edgesHead.hi {
		return s.edgesHead.target, true
	}
	for _, v := range s.edges {
		if e >= v.low && e <= v.hi {
			return v.target, true
		}
	}
	return 0, false
}

func (s *dfastate) AddEdge(e byte, target int) {
	// Initialize the head of the list of edges, if it hasn't already been
	// initialized.
	if s.edgesHead.hi < s.edgesHead.low {
		s.edgesHead = dfaedge{e, e, target}
		return
	}
	// Extend the head edge if possible.
	if s.edgesHead.target == target && s.edgesHead.hi == e-1 {
		s.edgesHead.hi = e
		return
	}

	// Try to merge edge with the last one in the slice.
	// This should merge all edges as they should be added in order
	l := len(s.edges)
	if l > 0 && s.edges[l-1].target == target && s.edges[l-1].hi == e-1 {
		s.edges[l-1].hi = e
	} else {
		s.edges = append(s.edges, dfaedge{e, e, target})
	}
}

type dfa []dfastate

func newDFA(nfa nfaNodePool, nfaStart int, handlers map[string]entry) (dfa, error) {
	// Create the start state
	start := []int{nfaStart}
	start = eps_closure(start, nfaStart, nfa.nodes)
	accept, err := determineAccept(start, nfa.nodes)
	if err != nil {
		return nil, err
	}
	states := []dfastate{{getHandler(handlers, accept), start, dfaedge{1, 0, 0}, nil}}

	lookup := map[string]int{buildKey(start): 0}

	// Convert the NFA states into DFA states.
	// Note that the calls to processState can append new DFA states to the
	// slice, so a standard range loop is not appropriate here.
	for i := 0; i < len(states); i++ {
		var err error
		states, err = processState(nfa.nodes, states, handlers, lookup, i)
		if err != nil {
			return nil, err
		}
	}

	// We no longer require information about the NFA states associated with
	// each DFA states.  Zero out the slice to release the memory.
	for i := range states {
		states[i].nfanodes = nil
	}

	return states, nil
}

func processState(nfa []nfaNode, states []dfastate, handlers map[string]entry, lookup map[string]int, n int) ([]dfastate, error) {
	for b := 0; b < 0x100; b++ {
		// Get set of nfa states reachable by byte from current set of nfa states
		target := get_nfa_target(nfa, states[n].nfanodes, byte(b))
		if len(target) == 0 {
			continue
		}
		target = get_eps_closure(nfa, target)

		// Does this set already exist
		key := buildKey(target)
		dstate, ok := lookup[key]
		if !ok {
			// This particlar set of states does not yet exist as a DFA state
			accept, err := determineAccept(target, nfa)
			if err != nil {
				return nil, err
			}
			states = append(states, dfastate{getHandler(handlers, accept), target, dfaedge{1, 0, 0}, nil})
			dstate = len(states) - 1
			lookup[key] = dstate
		}
		// Add the edge
		states[n].AddEdge(byte(b), dstate)
	}

	return states, nil
}

func get_nfa_target(nfa []nfaNode, nodes []int, edge byte) []int {
	var targetBuf [8]int
	target := targetBuf[:0]

	for _, v := range nodes {
		t2, ok := nfa[v].Move(edge)
		if ok {
			target = addState(target, t2)
		}
	}
	return target
}

func get_eps_closure(nfa []nfaNode, nodes []int) []int {
	closure := make([]int, len(nodes))
	copy(closure, nodes)
	for _, v := range nodes {
		closure = eps_closure(closure, v, nfa)
	}

	return closure
}

func getHandler(handlers map[string]entry, accept string) *handler {
	if accept == "" {
		return nil
	}
	entry, ok := handlers[accept]
	if !ok {
		panic("internal error")
	}
	return entry.handler
}
