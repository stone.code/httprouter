# Package httprouter

Package httprouter replaces the standard muxer provided by the standard
library with a more flexible method of specifying URLs.

This package provides another method of mutliplexing request.  A list of
regular expressions is registered with a builder.  When desired, this list
is compiled into a finite state machine that can match requests to a
specific target.  In additional to providing more flexibility in routing
requests, the regular expressions can verify the format of the URLs.
Malformed requests return a 404 error.

## Install

The package can be installed from the command line using the [go](https://golang.org/cmd/go/) tool.  There are no dependencies beyond the standard library.  Any version of Go should work, but testing only covers version 1.8 and up.

    go get gitlab.com/stone.code/httprouter

## Getting Started

Package documentation and examples are on [godoc](https://godoc.org/gitlab.com/stone.code/httprouter).

The syntax for the regular expressions matches the syntax of the regexp package in the standard library, and is also documented on [godoc](https://godoc.org/regexp/syntax). 

## Contribute

Feedback and PRs welcome.  You can also [submit an issue](https://gitlab.com/stone.code/httprouter/issues).

[![Go Report Card](https://goreportcard.com/badge/gitlab.com/stone.code/httprouter)](https://goreportcard.com/report/gitlab.com/stone.code/httprouter)

### Scope

The goals for this project is mainly to providing a replacement for the limited multiplexer provided by the standard library.  It also serves to test the unique approach of using regular expressions to building the multiplexer.  Otherwise, this package is intended to work with the standard library, and use [`http.Handler`](https://godoc.org/net/http#Handler) for serving requests.

## Related projects

- [github.com/gorilla/mux](https://godoc.org/github.com/gorilla/mux):  Package mux implements a request router and dispatcher.
- [github.com/julienschmidt/httprouter](https://godoc.org/github.com/julienschmidt/httprouter): Package httprouter is a trie based high performance HTTP request router.
- [go-http-routing-benchmark](https://github.com/julienschmidt/go-http-routing-benchmark):  Go HTTP request router and web framework benchmark

## License

BSD (c) Robert Johnstone