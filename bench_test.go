// Copyright 2015 Robert Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be found
// in the LICENSE file.
//
// Copied liberally from julienschmidt/go-http-routing-benchmark

package httprouter

import (
	"io"
	"net/http"
	"runtime"
	"testing"
)

// These support functions come from
func calcMem(name string, load func()) {
	m := new(runtime.MemStats)

	// before
	runtime.GC()
	runtime.ReadMemStats(m)
	before := m.HeapAlloc

	load()

	// after
	runtime.GC()
	runtime.ReadMemStats(m)
	after := m.HeapAlloc
	println("   "+name+":", after-before, "Bytes")
}

func benchRequest(b *testing.B, router http.Handler, r *http.Request) {
	w := new(mockResponseWriter)
	u := r.URL
	rq := u.RawQuery
	r.RequestURI = u.RequestURI()

	b.ReportAllocs()
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		u.RawQuery = rq
		router.ServeHTTP(w, r)
	}
}

func benchRoutes(b *testing.B, router http.Handler, routes []route) {
	w := new(mockResponseWriter)
	r, _ := http.NewRequest("GET", "/", nil)
	u := r.URL
	rq := u.RawQuery

	b.ReportAllocs()
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		for _, route := range routes {
			r.Method = route.method
			r.RequestURI = route.path
			u.Path = route.path
			u.RawQuery = rq
			router.ServeHTTP(w, r)
		}
	}
}

type route struct {
	method string
	path   string
}

type mockResponseWriter struct{}

func (m *mockResponseWriter) Header() (h http.Header) {
	return http.Header{}
}

func (m *mockResponseWriter) Write(p []byte) (n int, err error) {
	return len(p), nil
}

func (m *mockResponseWriter) WriteString(s string) (n int, err error) {
	return len(s), nil
}

func (m *mockResponseWriter) WriteHeader(int) {}

func httpHandlerFunc(w http.ResponseWriter, r *http.Request) {}

// Route with 1 Params (no write)
func BenchmarkParam(b *testing.B) {
	router := loadRouterSingle("GET", "/user/\\w+", http.HandlerFunc(httpHandlerFunc))

	r, _ := http.NewRequest("GET", "/user/gordon", nil)
	benchRequest(b, router, r)
}

// Route with 5 Params (no write)
func BenchmarkParam5(b *testing.B) {
	router := loadRouterSingle("GET", "/\\w+/\\w+/\\w+/\\w+/\\w+", http.HandlerFunc(httpHandlerFunc))

	r, _ := http.NewRequest("GET", "/test/test/test/test/test", nil)
	benchRequest(b, router, r)
}

// Route with 20 Params (no write)
func BenchmarkParam20(b *testing.B) {
	router := loadRouterSingle("GET", "/\\w+/\\w+/\\w+/\\w+/\\w+/\\w+/\\w+/\\w+/\\w+/\\w+/\\w+/\\w+/\\w+/\\w+/\\w+/\\w+/\\w+/\\w+/\\w+/\\w+", http.HandlerFunc(httpHandlerFunc))

	r, _ := http.NewRequest("GET", "/a/b/c/d/e/f/g/h/i/j/k/l/m/n/o/p/q/r/s/t", nil)
	benchRequest(b, router, r)
}

// Route with Param and write
func BenchmarkParamWrite(b *testing.B) {
	router := loadRouterSingle("GET", "/user/\\w+", http.HandlerFunc(routerHandleWrite))

	r, _ := http.NewRequest("GET", "/user/gordon", nil)
	benchRequest(b, router, r)
}

// Bridge functions between this package and the benchmark code supplied by julienschmidt/go-http-routing-benchmark

func handleFunc(builder *Builder, path string, method string) error {
	switch method {
	case "GET":
		return builder.HandleFunc(path, GetMethod, httpHandlerFunc)
	case "POST":
		return builder.HandleFunc(path, PostMethod, httpHandlerFunc)
	case "PUT":
		return builder.HandleFunc(path, PutMethod, httpHandlerFunc)
	case "DELETE":
		return builder.HandleFunc(path, GetMethod, httpHandlerFunc)
	default:
		panic("Unknow HTTP method: " + method)
	}
}

func loadRouter(routes []route) http.Handler {
	builder := NewBuilder()
	for _, route := range routes {
		err := handleFunc(builder, route.path, route.method)
		if err != nil {
			panic(err)
		}
	}

	mux, err := builder.NewServeMux()
	if err != nil {
		panic(err)
	}
	return mux
}

func routerHandleWrite(w http.ResponseWriter, r *http.Request) {
	_, _ = io.WriteString(w, UrlNth(r.URL, 1))
}

func loadRouterSingle(method, path string, handler http.Handler) http.Handler {
	builder := NewBuilder()
	err := handleFunc(builder, path, method)
	if err != nil {
		panic(err)
	}

	mux, err := builder.NewServeMux()
	if err != nil {
		panic(err)
	}
	return mux
}
