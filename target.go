// Copyright (c) 2012 Robert W Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package httprouter

import (
	"net/http"
)

// A Method specifies the method for an HTTP request.  Note that when used to
// specify the methods for a handler, the values can be combined.
type Method int

// The following constants are used to set which HTTP methods a handler will
// respond to.
const (
	GetMethod Method = (1 << iota)
	PostMethod
	PutMethod
	DeleteMethod
	AllMethods Method = GetMethod | PostMethod | PutMethod | DeleteMethod
)

// In addition to multiplexing results by URL, this package will also multiplex
// by method.  Users only need to implement those methods that they are
// interested in.
type handler struct {
	get    http.Handler
	post   http.Handler
	put    http.Handler
	delete http.Handler
}

// Overwrite the handler set by the method.
func (t *handler) update(method Method, handler http.Handler) {
	if method&GetMethod != 0 {
		t.get = handler
	}
	if method&PostMethod != 0 {
		t.post = handler
	}
	if method&PutMethod != 0 {
		t.put = handler
	}
	if method&DeleteMethod != 0 {
		t.delete = handler
	}
}

func (t *handler) method() Method {
	ret := Method(0)
	if t.get != nil {
		ret |= GetMethod
	}
	if t.post != nil {
		ret |= PostMethod
	}
	if t.put != nil {
		ret |= PutMethod
	}
	if t.delete != nil {
		ret |= DeleteMethod
	}
	return ret
}
