// Copyright (c) 2012 Robert W Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// +build graphviz

package httprouter

import (
	"fmt"
	"io"
)

func (n nfaNodePool) GraphVizNfa(w io.Writer) {
	fmt.Fprintf(w, "digraph{\n")

	for i, v := range n.nodes {
		if v.accept != "" {
			fmt.Fprintf(w, "n%d [ label=\"%d\", shape=doublecircle];\n", i+1, i+1)
		} else {
			fmt.Fprintf(w, "n%d [ label=\"%d\"];\n", i+1, i+1)
		}

		for _, e := range v.edges {
			fmt.Fprintf(w, "n%d -> n%d [label=\"%d-%d\"];\n", i+1, e.target+1, e.low, e.hi)
		}
		for _, target := range v.epsilons {
			fmt.Fprintf(w, "n%d -> n%d [label=\"eps\"];\n", i+1, target+1)
		}
	}

	fmt.Fprintf(w, "}\n")

}
