// Copyright (c) 2012 Robert W Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package httprouter

import (
	"regexp/syntax"
)

type nfaNodePool struct {
	nodes []nfaNode
}

func (p *nfaNodePool) NewEmptyNode() int {
	p.nodes = append(p.nodes, nfaNode{})
	id := len(p.nodes) - 1
	p.nodes[id].epsilons = p.nodes[id].epsilonsBuffer[:0]
	return id
}

func (p nfaNodePool) AddEdgeToNode(id int, low byte, hi byte, target int) {
	edge := nfaEdge{low, hi, target}
	p.nodes[id].edges = append(p.nodes[id].edges, edge)
}

func (p nfaNodePool) AddEpsToNode(id int, target int) {
	p.nodes[id].epsilons = append(p.nodes[id].epsilons, target)
}

type nfaEdge struct {
	low, hi byte
	target  int
}

type nfaNode struct {
	accept         string
	edges          []nfaEdge
	epsilons       []int
	epsilonsBuffer [2]int
}

func (n *nfaNode) Move(e byte) (int, bool) {
	for _, v := range n.edges {
		if e >= v.low && e <= v.hi {
			return v.target, true
		}
	}
	return 0, false
}

func newNFA(regexp *syntax.Regexp) (nodes nfaNodePool, start int, end int) {
	pool := nfaNodePool{make([]nfaNode, 0, 128)}

	start, end = newNFAWithPool(&pool, regexp.Simplify())
	return pool, start, end
}

func newNFAWithPool(pool *nfaNodePool, regexp *syntax.Regexp) (start, end int) {
	switch regexp.Op {
	case syntax.OpLiteral:
		start, end = newLiteralNFA(pool, regexp.Rune)
	case syntax.OpCharClass:
		start, end = newCharClassNFA(pool, regexp.Rune)
	case syntax.OpAnyCharNotNL:
		start, end = newAnyCharNotNLNFA(pool)
	case syntax.OpAnyChar:
		start, end = newAnyCharNFA(pool)
	case syntax.OpCapture:
		// We'll ignore the capture, but still process the expression
		start, end = newNFAWithPool(pool, regexp.Sub[0])
	case syntax.OpStar:
		start, end = newStarNFA(pool, regexp.Sub[0])
	case syntax.OpPlus:
		start, end = newPlusNFA(pool, regexp.Sub[0])
	case syntax.OpQuest:
		start, end = newQuestNFA(pool, regexp.Sub[0])
	case syntax.OpRepeat:
		// The documentation for syntax.Regexp.Simplify indicates that counted
		// reptitions will be rewritten using other operations.
		panic("syntax.OpRepeat should not appear in a simplified regexp")
	case syntax.OpConcat:
		start, end = newConcatNFA(pool, regexp.Sub)
	case syntax.OpAlternate:
		start, end = newAlternateNFA(pool, regexp.Sub)
	default:
		panic("unrecognized opcode in regexp AST")
	}

	if regexp.Name != "" {
		pool.nodes[end].accept = regexp.Name
	}

	return start, end
}

func newLiteralNFA(pool *nfaNodePool, literal []rune) (start, end int) {
	start = pool.NewEmptyNode()
	end = start

	for _, v := range literal {
		// TODO:  properly handle unicode
		tmp := pool.NewEmptyNode()
		pool.AddEdgeToNode(end, byte(v), byte(v), tmp)
		end = tmp
	}
	return start, end
}

func newCharClassNFA(pool *nfaNodePool, runes []rune) (start, end int) {
	start = pool.NewEmptyNode()
	end = pool.NewEmptyNode()

	for i := 0; i < len(runes); i = i + 2 {
		// TODO:  properly handle unicode
		pool.AddEdgeToNode(start, byte(runes[i]), byte(runes[i+1]), end)
	}
	return start, end
}

func newAnyCharNotNLNFA(pool *nfaNodePool) (start, end int) {
	// Note modified behaviour
	// Not NL means not '/' for URL parsing

	start = pool.NewEmptyNode()
	end = pool.NewEmptyNode()

	pool.AddEdgeToNode(start, 0, '/'-1, end)
	pool.AddEdgeToNode(start, '/'+1, 0xFF, end)

	return start, end
}

func newAnyCharNFA(pool *nfaNodePool) (start, end int) {
	start = pool.NewEmptyNode()
	end = pool.NewEmptyNode()

	pool.AddEdgeToNode(start, 0, 0xFF, end)

	return start, end
}

func newStarNFA(pool *nfaNodePool, regexp *syntax.Regexp) (start, end int) {
	// Get NFA for the sub expression
	start, end = newNFAWithPool(pool, regexp)

	// For zero times
	pool.AddEpsToNode(start, end)
	// For repeats
	pool.AddEpsToNode(end, start)

	return
}

func newPlusNFA(pool *nfaNodePool, regexp *syntax.Regexp) (start, end int) {
	// Get NFA for the sub expression
	start, end = newNFAWithPool(pool, regexp)
	// Add reverse link for repetition
	pool.AddEpsToNode(end, start)
	// Done
	return start, end
}

func newQuestNFA(pool *nfaNodePool, regexp *syntax.Regexp) (start, end int) {
	// Get NFA for the sub expression
	start, end = newNFAWithPool(pool, regexp)
	// Add forward link to create option
	pool.AddEpsToNode(start, end)
	// done
	return start, end
}

func newConcatNFA(pool *nfaNodePool, regexp []*syntax.Regexp) (start, end int) {
	if len(regexp) == 1 {
		start, end = newNFAWithPool(pool, regexp[0])
		return
	}

	start, end = newNFAWithPool(pool, regexp[0])
	for i := 1; i < len(regexp); i++ {
		// create nfa for next section
		start2, end2 := newNFAWithPool(pool, regexp[i])
		// join the expressions together
		pool.AddEpsToNode(end, start2)
		end = end2
	}
	return start, end
}

func newAlternateNFA(pool *nfaNodePool, regexp []*syntax.Regexp) (start, end int) {
	if len(regexp) == 1 {
		start, end = newNFAWithPool(pool, regexp[0])
		return
	}

	l := len(regexp)
	start1, end1 := newAlternateNFA(pool, regexp[:l/2])
	start2, end2 := newAlternateNFA(pool, regexp[l/2:])

	start = pool.NewEmptyNode()
	pool.AddEpsToNode(start, start1)
	pool.AddEpsToNode(start, start2)

	end = pool.NewEmptyNode()
	pool.AddEpsToNode(end1, end)
	pool.AddEpsToNode(end2, end)
	return start, end
}
