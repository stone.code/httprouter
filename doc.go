// Copyright (c) 2012 Robert W Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// Package httprouter replaces the standard muxer provided by the standard
// library with a more flexible method of specifying URLs.
//
// In the package net/http, the function ListenAndServe accepts a normal request
// handler.  This interface is called to create content for requests that the
// server receives.  Typically, a special handler is used to multiplex requests.
// The request multiplexer matches the URL of incoming requests against a list
// of registered handlers, and then dispatches that request as required.
//
// The multiplexer provided by the standard Go library matches requests by the
// URLs' prefixes.  This is a simple approach, but does not necessarily provide
// as much functionality as desired.
//
// This package provides another method of mutliplexing request.  A list of
// regular expressions is registered with a builder.  When desired, this list
// is compiled into a finite state machine that can match requests to a
// specific target.  In additional to providing more flexibility in routing
// requests, the regular expressions can verify the format of the URLs.
// Malformed requests return a 404 error.
//
// The syntax for the regular expressions matches the syntax of the regexp
// package in the standard library (see regexpr/syntax).
package httprouter
