// Copyright (c) 2012 Robert W Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// +build graphviz

package httprouter

import (
	"fmt"
	"io"
)

func (n dfa) GraphViz(w io.Writer) {
	fmt.Fprintf(w, "digraph{\n")

	for i, v := range n {
		if v.handler != nil {
			fmt.Fprintf(w, "n%d [ label=\"%d\", shape=doublecircle];\n", i+1, i+1)
		} else {
			fmt.Fprintf(w, "n%d [ label=\"%d\"];\n", i+1, i+1)
		}

		fmt.Fprintf(w, "n%d -> n%d [label=\"%d-%d\"];\n", i+1, v.edgesHead.target+1, v.edgesHead.low, v.edgesHead.hi)
		for _, e := range v.edges {
			fmt.Fprintf(w, "n%d -> n%d [label=\"%d-%d\"];\n", i+1, e.target+1, e.low, e.hi)
		}
	}

	fmt.Fprintf(w, "}\n")

}
