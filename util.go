// Copyright (c) 2012 Robert W Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package httprouter

import (
	"net/url"
)

// PathNth returns the nth element of the path.  If the element is found, it
// is returned without the leading or trailing slashes.
//
// The first element has an index of zero.  If the path has a leading slash,
// it is ignored.  Therefore, the leading elements (element 0) of "/a/b/c" and
// "a/b/c" are both "a".
//
// The last element in the path may contain a trailing slash, but it is not
// required.  For example, the final elements (element 2) of "/a/b/c/" and
// "/a/b/c" are both "c".
func PathNth(path string, nth int) string {
	// Skip a leading slash.  It is not counted
	if path[0] == '/' {
		path = path[1:]
	}
	// Iterate over element of the path (each element is \w*/).
	for nth > 0 && len(path) > 0 {
		if path[0] == '/' {
			nth--
		}
		path = path[1:]
	}
	// Find the terminating slash, if it exists
	for end := 0; end < len(path); end++ {
		if path[end] == '/' {
			return path[:end]
		}
	}
	return path
}

// UrlNth returns the nth element of the URL's path.
// Refer to PathNth for more details.
func UrlNth(url *url.URL, nth int) string {
	return PathNth(url.Path, nth)
}
