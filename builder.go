// Copyright (c) 2012 Robert W Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package httprouter

import (
	"errors"
	"fmt"
	"net/http"
	"regexp/syntax"
)

var (
	// ErrBuilderIsEmpty indicates that the Builder is empty, and so cannot build a ServeMux.
	ErrBuilderIsEmpty = errors.New("failed to create ServeMux because Builder is empty")
)

type entry struct {
	pattern string
	handler *handler
	regexp  *syntax.Regexp
}

// A Builder maintains a list of regular expressions, and the HTTP handlers that
// should be executed when one of those expressions matches the URL of an HTTP
// request.  Taken together, the regular expressions describe the routing of
// requests to their handlers.
//
// The Builder is not used directly.  Instead, once the expressions have all
// been added, users should call the method NewServeMux to create a request
// multiplexer that can be used to serve HTTP requests.
type Builder struct {
	entries map[string]entry
}

// NewBuilder initializes a new Builder.
func NewBuilder() *Builder {
	return &Builder{make(map[string]entry)}
}

// Handle registers the handler for the given method and the given regular
// expression.  If the regular expression already exists, the handlers for those
// methods specified by the parameter method are overwritten.
//
// The pattern is a regular expression conforming to the syntax of the package
// regexp.  The expression must be valid.  If it fails to compile, this method
// will panic.
//
// The use of '.' to match any character is modified from package regexpr.
// In this package, this operator will match any character except a forward
// slash, and so will not capture multiple path elements.
func (n *Builder) Handle(pattern string, method Method, requestHandler http.Handler) error {
	// Get the entry for this pattern
	v := n.entries[pattern]

	// update
	if v.pattern == "" {
		v.pattern = pattern
		var err error
		v.regexp, err = syntax.Parse(pattern, syntax.Perl)
		if err != nil {
			return err
		}
		// Hack.  Use the name field, used for OpCapture, to hold the
		// pattern.  Will need this information when we build the NFA
		// to direct matches to the correct target.
		v.regexp.Name = pattern

		v.handler = &handler{}

	}
	v.handler.update(method, requestHandler)

	// Store update entry
	n.entries[pattern] = v
	// and we are done
	return nil
}

// MustHandle is like the method Handle, but panics if the pattern cannot be
// parsed. It simplifies safe initialization of variables holding known regular
// expressions.
func (n *Builder) MustHandle(pattern string, method Method, requestHandler http.Handler) {
	err := n.Handle(pattern, method, requestHandler)
	if err != nil {
		panic(fmt.Sprintf("httprouter: handle(%q): %s", pattern, err.Error()))
	}
}

// HandleFunc registers the handler function for the given method and the given
// regular expression.  See the method Handle for further details.
func (n *Builder) HandleFunc(pattern string, method Method, handler func(http.ResponseWriter, *http.Request)) error {
	return n.Handle(pattern, method, http.HandlerFunc(handler))
}

// MustHandleFunc is like the method HandleFunc, but panics if the pattern
// cannot be parsed.  It simplifies safe initialization of variables holding
// known regular expressions.
func (n *Builder) MustHandleFunc(pattern string, method Method, handler func(http.ResponseWriter, *http.Request)) {
	n.MustHandle(pattern, method, http.HandlerFunc(handler))
}

// NewServeMux compiles the list of regular expressions into a machine, and
// returns a new request multiplexer that will dispatch HTTP requests.
func (n *Builder) NewServeMux() (mux *ServeMux, err error) {
	// Make sure that we have at least one pattern
	if len(n.entries) == 0 {
		// Not sure if we should return an error,
		// or return a mux that simply 404's every request
		return nil, ErrBuilderIsEmpty
	}

	sub := []*syntax.Regexp{}
	for _, v := range n.entries {
		sub = append(sub, v.regexp)
	}

	regexp := &syntax.Regexp{
		Op:    syntax.OpAlternate,
		Flags: syntax.Perl,
		Sub:   sub}

	nfa, start, _ := newNFA(regexp)
	dfa, err := newDFA(nfa, start, n.entries)
	if err != nil {
		return nil, err
	}

	return &ServeMux{
		dfa,
		NewErrorHandler(http.StatusNotFound),
		NewErrorHandler(http.StatusMethodNotAllowed),
	}, nil
}
