// Copyright (c) 2012 Robert W Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package httprouter

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
)

func ExampleServeMux() {
	// Create a builder to start construction of our routing policy.
	b := NewBuilder()

	// The login page will be special.  A get method will return an HTML
	// login page, but we'll post our credentials to login.
	b.MustHandleFunc("/login", GetMethod, func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "<html><body>A login page.</body></html>")
	})

	mux, err := b.NewServeMux()
	if err != nil {
		// Omitting error handling in the example.
		panic(err)
	}
	server := httptest.NewServer(mux)
	defer server.Close()

	resp, err := http.Get(server.URL + "/login")
	if err != nil {
		// Omitting error handling in the example.
		panic(err)
	}
	defer resp.Body.Close()

	buffer, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		// Omitting error handling in the example.
		panic(err)
	}

	fmt.Printf("%s\n", buffer)

	// Output:
	// <html><body>A login page.</body></html>
}

func ExampleServeMux_param() {
	// Create a builder to start construction of our routing policy.
	b := NewBuilder()

	// Our API takes a single parameter, which is the second element of the
	// path.  Note the use of ".+" to allow any content for the parameter, and
	// that the trailing slash is optional.
	b.MustHandleFunc("/api/.+/?", GetMethod, func(w http.ResponseWriter, r *http.Request) {
		param := UrlNth(r.URL, 1)
		fmt.Fprintf(w, "Hello, %s!", param)
	})

	mux, err := b.NewServeMux()
	if err != nil {
		// Omitting error handling in the example.
		panic(err)
	}
	server := httptest.NewServer(mux)
	defer server.Close()

	resp, err := http.Get(server.URL + "/api/world")
	if err != nil {
		// Omitting error handling in the example.
		panic(err)
	}
	defer resp.Body.Close()

	buffer, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		// Omitting error handling in the example.
		panic(err)
	}

	fmt.Printf("%s\n", buffer)

	// Output:
	// Hello, world!
}

func ExampleServeMux_path() {
	// Create a builder to start construction of our routing policy.
	b := NewBuilder()

	// Allow an arbitrary trailing path to match.  Any number of elements are
	// allowed in the path.  Note the use of a flag to allow . to also match
	// forward slashes. The trailing slash is optional.
	b.MustHandleFunc("/api/(?s:.+)", GetMethod, func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "path: %s", r.URL.Path[5:])
	})

	mux, err := b.NewServeMux()
	if err != nil {
		// Omitting error handling in the example.
		panic(err)
	}
	server := httptest.NewServer(mux)
	defer server.Close()

	resp, err := http.Get(server.URL + "/api/a/path/to/a/file")
	if err != nil {
		// Omitting error handling in the example.
		panic(err)
	}
	defer resp.Body.Close()

	buffer, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		// Omitting error handling in the example.
		panic(err)
	}

	fmt.Printf("%s\n", buffer)

	// Output:
	// path: a/path/to/a/file
}

func TestServeMux_match(t *testing.T) {
	b := NewBuilder()

	b.MustHandleFunc("/login", GetMethod, func(w http.ResponseWriter, r *http.Request) {
		println("Dummy method")
	})
	b.MustHandleFunc("/logout", PostMethod, func(w http.ResponseWriter, r *http.Request) {
		println("Dummy method")
	})
	b.MustHandleFunc("/ab+", PutMethod, func(w http.ResponseWriter, r *http.Request) {
		println("Dummy method")
	})
	b.MustHandleFunc("/cd*", DeleteMethod, func(w http.ResponseWriter, r *http.Request) {
		println("Dummy method")
	})
	b.MustHandleFunc("/e{3,4}", GetMethod|PostMethod, func(w http.ResponseWriter, r *http.Request) {
		println("Dummy method")
	})

	mux, err := b.NewServeMux()
	if err != nil {
		t.Errorf("Error occurred while building the mux.")
	}
	if mux == nil {
		t.Errorf("Returned mux is nil.")
	}

	cases := []struct {
		in     string
		ok     bool
		method Method
	}{
		{"/login", true, GetMethod},
		{"/logout", true, PostMethod},
		{"/logi", false, 0},
		{"/login-", false, 0},
		{"/abbb", true, PutMethod},
		{"/a", false, 0},
		{"/cddd", true, DeleteMethod},
		{"/c", true, DeleteMethod},
		{"/ee", false, 0},
		{"/eee", true, GetMethod | PostMethod},
		{"/eeee", true, GetMethod | PostMethod},
		{"/eeeee", false, 0},
	}

	for _, v := range cases {
		t.Run(v.in, func(t *testing.T) {
			h := mux.match(v.in)

			if ok := h != nil; ok != v.ok {
				t.Errorf("Failed match return: got %v, want %v", ok, v.ok)
			}
			if h != nil {
				if method := h.method(); method != v.method {
					t.Errorf("Incorrect method for handler: got %v, want %v", method, v.method)
				}
			}
		})
	}
}

func getBody(url string) (body []byte, code int, err error) {

	resp, err := http.Get(url)
	if err != nil {
		return nil, 0, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, resp.StatusCode, nil
	}

	buffer, err := ioutil.ReadAll(resp.Body)
	return buffer, resp.StatusCode, nil
}

func TestServeMux(t *testing.T) {

	b := NewBuilder()
	b.MustHandleFunc("/a+/?", GetMethod, func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "<html><body><p>%s</p></body></html>", r.URL.Path)
	})
	b.MustHandleFunc("/(b|c)(d|e)/?", GetMethod, func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "<html><body><div>%s</div></body></html>", r.URL.Path)
	})
	b.MustHandleFunc("/class/[[:alpha:]]+", GetMethod, func(w http.ResponseWriter, r *http.Request) {
		param := UrlNth(r.URL, 1)
		fmt.Fprintf(w, "<html><body><div>%s</div></body></html>", param)
	})

	mux, err := b.NewServeMux()
	if err != nil {
		t.Fatalf("Error occurred while building the mux.")
	}
	if mux == nil {
		t.Fatalf("Returned mux is nil.")
	}
	server := httptest.NewServer(mux)
	defer server.Close()

	cases := []struct {
		url  string
		ok   bool
		resp string
	}{
		{"/aaaa", true, "<html><body><p>/aaaa</p></body></html>"},
		{"/be", true, "<html><body><div>/be</div></body></html>"},
		{"/cd", true, "<html><body><div>/cd</div></body></html>"},
		{"/bf", false, ""},
		{"/class/world", true, "<html><body><div>world</div></body></html>"},
	}

	for _, v := range cases {
		t.Run(v.url, func(t *testing.T) {
			buffer, code, err := getBody(server.URL + v.url)
			if err != nil {
				t.Errorf("Error during GET:  %s", err)
			}
			if v.ok {
				if code != http.StatusOK {
					t.Errorf("Unexpected HTTP status for %s:  %d", v.url, code)
				} else if string(buffer) != v.resp {
					t.Errorf("Incorrect body for %s", v.url)
				}
			} else {
				if code != http.StatusNotFound {
					t.Errorf("Unexpected HTTP status for %s:  %d", v.url, code)
				}
			}
		})
	}
}

func TestServeMux2(t *testing.T) {

	b := NewBuilder()
	b.MustHandleFunc("/a+/?", GetMethod, func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "<html><body><p>%s</p></body></html>", r.URL.Path)
	})
	b.MustHandleFunc("/(b|c)(d|e)/?", PutMethod, func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "<html><body><div>%s</div></body></html>", r.URL.Path)
	})
	b.MustHandleFunc("/d/?", DeleteMethod, func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "deleted %s", r.URL.Path)
	})
	b.MustHandleFunc("/p/?", PostMethod, func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "post %s", r.URL.Path)
	})

	mux, err := b.NewServeMux()
	if err != nil {
		t.Fatalf("Error occurred while building the mux.")
	}
	if mux == nil {
		t.Fatalf("Returned mux is nil.")
	}
	server := httptest.NewServer(mux)
	defer server.Close()

	cases := []struct {
		url    string
		method string
		resp   string
	}{
		{"/aaaa", "GET", "<html><body><p>/aaaa</p></body></html>"},
		{"/aaaa", "PUT", "405 Method Not Allowed\n"},
		{"/aaaa", "DELETE", "405 Method Not Allowed\n"},
		{"/aaaa", "POST", "405 Method Not Allowed\n"},
		{"/aaaa", "DUMMY", "405 Method Not Allowed\n"},
		{"/be", "PUT", "<html><body><div>/be</div></body></html>"},
		{"/cd", "PUT", "<html><body><div>/cd</div></body></html>"},
		{"/cd", "GET", "405 Method Not Allowed\n"},
		{"/d", "DELETE", "deleted /d"},
		{"/p", "POST", "post /p"},
		{"/p/", "POST", "post /p/"},
		{"/dne/", "GET", "404 Not Found\n"},
	}

	for _, v := range cases {
		t.Run(v.url, func(t *testing.T) {
			req, err := http.NewRequest(v.method, server.URL+v.url, nil)
			if err != nil {
				t.Fatalf("Error creating request:  %s", err)
			}
			resp, err := http.DefaultClient.Do(req)
			if err != nil {
				t.Fatalf("Error during request:  %s", err)
			}
			defer resp.Body.Close()

			buffer, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				t.Errorf("Error reading request:  %s", err)
			}
			if string(buffer) != v.resp {
				t.Logf("Response: %s", buffer)
				t.Errorf("Incorrect body for %s", v.url)
			}
		})
	}
}
