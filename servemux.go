// Copyright (c) 2012 Robert W Johnstone. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package httprouter

import (
	"fmt"
	"net/http"
	"path"
)

// A ServeMux is a request multiplexer.  Incoming HTTP requests are matches
// against a list of regular expressions, and the request is dispatched to the
// appropriate handler.
//
// The multiplexer contains two special handler for handling errors.  These are
// otherwise normal handlers, but users should not that the HTTP headers have
// already been sent when they are called.
type ServeMux struct {
	dfa dfa

	// NotFoundHandler is called to write error messages for 404 code
	// errors.
	NotFoundHandler http.Handler
	// MethodNotAllowedHandler is called to write error messages for 405
	// code errors.
	MethodNotAllowedHandler http.Handler
}

// Return the canonical path for p, eliminating . and .. elements.
// Copied from the net/http.cleanPath
func cleanPath(p string) string {
	if p == "" {
		return "/"
	}
	if p[0] != '/' {
		p = "/" + p
	}
	np := path.Clean(p)
	// path.Clean removes trailing slash except for root;
	// put the trailing slash back if necessary.
	if p[len(p)-1] == '/' && np != "/" {
		np += "/"
	}
	return np
}

func (m *ServeMux) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	// The following block is copied from the net/http.ServerMux
	if r.Method != "CONNECT" {
		// Clean path to canonical form and redirect
		if p := cleanPath(r.URL.Path); p != r.URL.Path {
			w.Header().Set("Location", p)
			w.WriteHeader(http.StatusMovedPermanently)
			return
		}
	}

	// We do not need to lock the dfa, as it is never modifed once
	// the ServeMux has been created.

	// Select the entry
	h := m.match(r.Host + r.URL.Path)
	if h == nil {
		h = m.match(r.URL.Path)
	}
	if h == nil {
		m.NotFoundHandler.ServeHTTP(w, r)
		return
	}

	switch r.Method {
	case "GET":
		if h.get == nil {
			m.MethodNotAllowedHandler.ServeHTTP(w, r)
			return
		}
		h.get.ServeHTTP(w, r)
		return
	case "POST":
		if h.post == nil {
			m.MethodNotAllowedHandler.ServeHTTP(w, r)
			return
		}
		h.post.ServeHTTP(w, r)
		return
	case "PUT":
		if h.put == nil {
			m.MethodNotAllowedHandler.ServeHTTP(w, r)
			return
		}
		h.put.ServeHTTP(w, r)
		return
	case "DELETE":
		if h.delete == nil {
			m.MethodNotAllowedHandler.ServeHTTP(w, r)
			return
		}
		h.delete.ServeHTTP(w, r)
		return
	}

	m.MethodNotAllowedHandler.ServeHTTP(w, r)
}

// match runs the URL against the state machine, and returns the handler that
// matches, or nil.
func (m *ServeMux) match(url string) *handler {
	state := 0

	for len(url) > 0 {
		c := url[0]
		nextState, ok := m.dfa[state].Move(c)
		if !ok {
			return nil
		}
		url = url[1:]
		state = nextState
	}

	return m.dfa[state].handler
}

// A ErrorHandler writes an error message to the HTTP response with the error
// code and the associated error text.  Users should initialize the structure
// with a standard HTTP error code.
type ErrorHandler struct {
	code int
}

// NewErrorHandler creates a new ErrorHandler.
func NewErrorHandler(code int) *ErrorHandler {
	return &ErrorHandler{code: code}
}

// ServeHTTP write reply headers for the error code, and creates a short
// response with text that describes the error.
func (h *ErrorHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	http.Error(w,
		fmt.Sprintf("%d %s", h.code, http.StatusText(h.code)),
		h.code)
}
